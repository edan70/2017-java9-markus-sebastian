ExtendJ Java 9
======================

This project is a an extension implementing parts of Java 9 to the compiler ExtendJ.

Cloning this Project
--------------------

To clone this project you will need [Git][2] installed.

Use this command to clone the project using Git:

    git clone --recursive https://bitbucket.org/edan70/2017-java9-markus-sebastian.git

The `--recursive` flag makes Git also clone the ExtendJ submodule while cloning
the repository.

If you forgot the `--recursive` flag you can manually clone the ExtendJ
submodule using these commands:

    cd 2017-java9-markus-sebastian
    git submodule init
    git submodule update

This should download the ExtendJ Git repository into a local directory named
`extendj`.

Build and Run
-------------

If you have [Gradle][1] installed you can issue the following commands to
build and test the compiler:

    gradle jar
    ./run_tests.sh

The script will run all tests. Each feature has tests in a separate folder, inside the testfiles folder. Tests in folders ending with a "p" should compile, and those ending with "f" should get compile errors. All tests should pass.

If you do not have Gradle installed you can use the `gradlew.bat` (on Windows)
or `gradlew` (Mac/Linux) script instead. For example to build on Windows run the
following in a command prompt:

    gradlew jar

The `gradlew` scripts are wrapper scripts that will download Gradle locally and
run it.

License
-------
This repository is covered by the license BSD 2-clause, see file LICENSE.

Credits
-------
This repository is based on a repository by Jesper Öqvist, licensed under BSD 2-clause.


[1]:https://gradle.org/
[2]:https://git-scm.com/
