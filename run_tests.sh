
BIN=bin  # Relative to script folder
COMPILER="extendj-java9.jar"

RED='\033[0;31m'
NC='\033[0m' # No Color

process_test()  # $1 is test path
{
	prev=$PWD
	test_name=${1%*/}
	compiler="$prev/../$COMPILER"
	bin="$prev/../$BIN"
	target=${1: -2:-1}
	cd $1

	java -jar "$compiler" -d "$bin" Test.java 2> "$bin/out.txt"
	cat "$bin/out.txt" | grep -v warning > "$bin/out2.txt"
	mv "$bin/out2.txt" "$bin/out.txt"

	if [[ -s "$bin/out.txt" && "$target" != "f" || ! -s "$bin/out.txt" && "$target" != "p" ]] ; then
		printf "${RED}FAILED:${NC} $test_name\n"
	else
		printf "Passed: $test_name\n"
	fi

	cd "$prev"
}

if [[ ! -d "$BIN" ]] ; then
	mkdir "$BIN"
fi

cd testfiles
for dir in **/*/
do
	process_test $dir
done
