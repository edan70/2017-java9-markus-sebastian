package testfiles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestTWR9 {

	public static void main(String[] args) throws IOException {
		BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
		try (r) {
			System.out.println(r.readLine());
		} catch (IOException e) {
		}
		System.out.println(r.read());
	}
}
