package testfiles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class TWRTestCompiles {


	private void hej() {

		try (BufferedReader a = new BufferedReader(null); Reader r = null;) {
		}catch (IOException e) {
		}

		final BufferedReader a = new BufferedReader(null);
		try (a) {
			a.read();
		}catch (IOException e) {
		}

		BufferedReader b = new BufferedReader(null);
		try (b) {
		}catch (IOException e) {
		}

		try (b; Reader c = new BufferedReader(null)) {
		}catch (IOException e) {
		}

		try (Reader a2 = a; Reader a3 = a) {
		}catch (IOException e) {
		}

		BufferedReader d = new BufferedReader(null);
		try (d) {
		}catch (IOException e) {
		}

		BufferedReader f = new BufferedReader(null);
		try (f) {
		}catch (IOException e) {
		}
	}

	public static void main(String[] args) {
		new TWRTestCompiles().hej();
	}
}
