// @SafeVarargs is not allowed on a non-static, non-final, non-private method.
// .result: COMPILE_FAIL

public class SafeVarargsTestFail {
	
	@SafeVarargs
	public int m1(String... strings) {
		return strings.length;
	}
}
