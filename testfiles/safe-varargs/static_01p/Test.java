import java.util.Arrays;
import java.util.List;

public class SafeVarargsTest {
	
	@SafeVarargs // Not actually safe!
	static String m1(List<String>... stringLists) {
		Object[] array = stringLists;
		List<Integer> tmpList = Arrays.asList(42);
		array[0] = tmpList; // Semantically invalid, but compiles without warnings
		String s = stringLists[0].get(0); // Oh no, ClassCastException at runtime!
		return s;
	}
}
