// @SafeVarargs is not allowed on a non-variable-arity methods
// .result: COMPILE_FAIL

public class SafeVarargsTestFail {

	@SafeVarargs
	private int m2() {
		return 0;
	}
}
