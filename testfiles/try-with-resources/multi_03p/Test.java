

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class Test {

	private void hej() {
		BufferedReader a = new BufferedReader(null);
		try (Reader a2 = a; Reader a3 = a) {
			a2.read();
			a3.read();
		}catch (IOException e) {
		}
	}
}
