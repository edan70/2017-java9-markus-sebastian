// Resource can not be a field
// .result: COMPILE_FAIL

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class Test {

	private final BufferedReader reader = null;

	private void hej() {
		try (reader) {
			reader.read();
		} catch (IOException e){
		}
	}
}
