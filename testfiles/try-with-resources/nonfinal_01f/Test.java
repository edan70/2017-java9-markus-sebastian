// Resource must be final or effectively final
// .result: COMPILE_FAIL

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class Test {

	private void hej() {
		BufferedReader c = new BufferedReader(null);
		c = new BufferedReader(null);
		try (c) {
		}catch (IOException e) {
		}

	}
}
