// Resource must be of type AutoCloseable
// .result: COMPILE_FAIL

import java.io.IOException;

public class Test {

	private void hej() {
		int i = 0;
		try (i) {
		} catch (Exception e) {
		}
	}
}
