package testfiles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class TWRTestErrors {


	private final BufferedReader reader = null;

	private void hej() {
		try (reader) {
			reader.read();
		} catch (IOException e){
		}

		int i = 0;
		try (i) {
		} catch (Exception e) {
		}

		BufferedReader c = new BufferedReader(null);
		c = new BufferedReader(null);
		try (c) {
		}catch (IOException e) {
		}

		BufferedReader d = new BufferedReader(null);
		try (d) {
			d = null;
		}catch (IOException e) {
		}

		BufferedReader f = new BufferedReader(null);
		try (f) {
		}catch (IOException e) {
		}
		f = null;

	}

	public static void main(String[] args) {
		new TWRTestErrors().hej();
	}
}
