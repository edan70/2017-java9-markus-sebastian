package testfiles;

import java.util.Arrays;
import java.util.List;

public class SafeVarargsTest {
	
	@SafeVarargs
	public SafeVarargsTest(String... stringLists) {
	}
	
	@SafeVarargs // Not actually safe!
	static String m1(List<String>... stringLists) {
		Object[] array = stringLists;
		List<Integer> tmpList = Arrays.asList(42);
		array[0] = tmpList; // Semantically invalid, but compiles without warnings
		String s = stringLists[0].get(0); // Oh no, ClassCastException at runtime!
		return s;
	}
	
	@SafeVarargs // Not actually safe!
	final String m2(List<String>... stringLists) {
		Object[] array = stringLists;
		List<Integer> tmpList = Arrays.asList(42);
		array[0] = tmpList; // Semantically invalid, but compiles without warnings
		String s = stringLists[0].get(0); // Oh no, ClassCastException at runtime!
		return s;
	}
	
	@SafeVarargs // Not actually safe!
	private String m3(List<String>... stringLists) {
		Object[] array = stringLists;
		List<Integer> tmpList = Arrays.asList(42);
		array[0] = tmpList; // Semantically invalid, but compiles without warnings
		String s = stringLists[0].get(0); // Oh no, ClassCastException at runtime!
		return s;
	}
	
	public static void main(String[] args) {
		try {
			m1(Arrays.asList("m1"));
		} catch (ClassCastException e) {
		}
		SafeVarargsTest test = new SafeVarargsTest();
		try {
			test.m2(Arrays.asList("m2"));
		} catch (ClassCastException e) {
		}
		try {
			test.m3(Arrays.asList("m3"));
		} catch (ClassCastException e) {
		}
		System.out.println("It works!");
	}
	
//	@SafeVarargs
//	public int m(String... strings) {
//		return strings.length;
//	}
	
//	@SafeVarargs
//	private int m() {
//		return 0;
//	}
	
//	@SafeVarargs // Not actually safe!
//	public String m4(List<String>... stringLists) {
//		Object[] array = stringLists;
//		List<Integer> tmpList = Arrays.asList(42);
//		array[0] = tmpList; // Semantically invalid, but compiles without warnings
//		String s = stringLists[0].get(0); // Oh no, ClassCastException at runtime!
//		return s;
//	}
}
