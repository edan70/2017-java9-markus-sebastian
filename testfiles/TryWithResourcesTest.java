package testfiles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class TryWithResourcesTest {
	
	// Commented lines does not compile with javac-9

	private void da() {
		try (BufferedReader a = new BufferedReader(null)) {
			a.read();
		} catch (IOException e) {
		}
	}
	
	private final BufferedReader reader = null;
	
	private void hej() {
		try (reader) {
			reader.read();
		} catch (IOException e){
		}
		
		int i = 0;
		try (i) {
		} catch (Exception e) {
		}
		
		try (BufferedReader a = new BufferedReader(null); Reader r = null;) {
		}catch (IOException e) {
		}

		// try (BufferedReader b) {
			// b = new BufferedReader(null);
		// }catch (IOException e) {
		// }
		
		final BufferedReader a = new BufferedReader(null);
		try (a) {
			a.read();
		}catch (IOException e) {
		}
		
		BufferedReader b = new BufferedReader(null);
		try (b) {
		}catch (IOException e) {
		}
		
		try (b; Reader c = new BufferedReader(null)) {
		}catch (IOException e) {
		}

		try (Reader a2 = a; Reader a3 = a) {
		}catch (IOException e) {
		}
		
		BufferedReader c = new BufferedReader(null);
		c = new BufferedReader(null);
		 try (c) {
		 }catch (IOException e) {
		 }
		
		BufferedReader d = new BufferedReader(null);
		try (d) {
			// d = null;
		}catch (IOException e) {
		}
		
		// try (b;;) {
		// }catch (IOException e) {
		// }
		
		// try (;b) {
		// }catch (IOException e) {
		// }
		
		BufferedReader f = new BufferedReader(null);
		try (f) {
		}catch (IOException e) {
		}
		// f = null;
		
		// try (Reader g) {
			// g = null;
		// }catch (IOException e) {
		// }
		
		// BufferedReader g;
		// try (g = new BufferedReader(null)) {
		// }catch (IOException e) {
		// }
	}
	
	public static void main(String[] args) {
		new TryWithResourcesTest().hej();
	}
}
