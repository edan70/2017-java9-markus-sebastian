  
/* 3.8 Identifiers
 * located at end of current state due to rule priority disambiguation
 */
<YYINITIAL> {
  (("_")({jletterdigit})+|({jletter})({jletterdigit})*) { return sym(Terminals.IDENTIFIER); }
}
