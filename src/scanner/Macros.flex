
// Any allowed java identifier part 
jletterdigit = ([:jletterdigit:]|[\ud800-\udfff])
// Any allowed java identifier start except _
jletter = !(!([:jletter:]|[\ud800-\udfff])|"_")
